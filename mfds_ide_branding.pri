QTCREATOR_VERSION = 0.92.0
QTCREATOR_COMPAT_VERSION = 0.92.0
QTCREATOR_DISPLAY_VERSION = 0.6.0-beta
QTCREATOR_COPYRIGHT_YEAR = 2021

IDE_DISPLAY_NAME = MFDS-qc
IDE_ID = milab
IDE_CASED_ID = MILab

PRODUCT_BUNDLE_ORGANIZATION = org.milab
PROJECT_USER_FILE_EXTENSION = .user

IDE_DOC_FILES_ONLINE = $$PWD/doc/mfds/mfds-online.qdocconf \
                       $$PWD/doc/mfds_dev/mfds_dev-online.qdocconf
IDE_DOC_FILES = $$PWD/doc/mfds/mfds.qdocconf \
                $$PWD/doc/mfds_dev/mfds_dev.qdocconf
