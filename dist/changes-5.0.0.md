MFDS-qc
===============

MFDS-qc contains bug fixes and new features.

The most important changes are listed in this document. For a complete list of
changes, see the Git log for the MFDS-qc sources that you can check out from
the Git repository. For example:

    git clone <server_address>
    git log --cherry-pick --pretty=oneline origin/4.15..v5.0.0

General
-------

* Fixed various possible crashes at ...

Projects
--------

* Added experimental support for ...

Platforms
---------

### Windows

* Added support for ...

### macOS

* Fixed performance issue with ...


Credits for these changes go to:
--------------------------------

