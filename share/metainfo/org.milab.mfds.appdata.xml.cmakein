<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (C) 2021 MILab. Contact: milab@gmail.com -->
<component type="desktop">
 <id>org.milab.mfds.desktop</id>
 <name>MILab</name>
 <summary>Provides a cross-platform, complete integrated development environment (IDE) for application developers to create applications for multiple platforms and devices</summary>
 <metadata_license>CC0-1.0</metadata_license>
 <project_license>GPL-3.0</project_license>
 <description>
  <p>
   MILab provides a cross-platform, complete integrated development
   environment (IDE) for application developers to create applications for
   multiple desktop and mobile device platforms, such as Android and iOS.
  </p>
  <p>
   MILab enables a team of developers to share a project across different
   development platforms with a common tool for development and debugging.
  </p>
  <p>
   The main goal for MILab is meeting the development needs of MILab
   developers who are looking for simplicity, usability, productivity,
   extendibility and openness, while aiming to lower the barrier of entry for
   newcomers to MILab.
  </p>
 </description>

 <url type="homepage">https://scumed.machineilab.org/</url>
 <project_group>MILab</project_group>
 <content_rating type="oars-1.1" />
 <screenshots>
  <screenshot>
   <image>https://scumed.machineilab.org/</image>
  </screenshot>
  <screenshot>
   <image>https://scumed.machineilab.org/</image>
  </screenshot>
 </screenshots>
 <releases>
  <release version="${IDE_VERSION_DISPLAY}"${DATE_ATTRIBUTE}>
   <description>
    <p>MILab v${IDE_VERSION_DISPLAY}</p>
   </description>
  </release>
 </releases>
</component>
