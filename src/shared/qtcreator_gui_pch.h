/*
 * This is a precompiled header file for use in Xcode / Mac GCC /
 * GCC >= 3.4 / VC to greatly speed the building of Qt Creator.
 */
#ifndef QTCREATOR_GUI_PCH_H
#define QTCREATOR_GUI_PCH_H

#include "qtcreator_pch.h"

#if defined __cplusplus

#include <QEvent>
#include <QTimer>
#include <QApplication>
#include <QBitmap>
#include <QCursor>
#include <QImage>
#include <QLayout>
#include <QPainter>
#include <QPixmap>
#include <QStyle>
#include <QWidget>

#endif

#endif // QTCREATOR_GUI_PCH_H
