#pragma once

#include "core_global.h"

#include <utils/icon.h>

namespace Core {
namespace Icons {

CORE_EXPORT extern const Utils::Icon QTCREATORLOGO_BIG;
CORE_EXPORT extern const Utils::Icon FIND_CASE_INSENSITIVELY;
CORE_EXPORT extern const Utils::Icon FIND_WHOLE_WORD;
CORE_EXPORT extern const Utils::Icon FIND_REGEXP;
CORE_EXPORT extern const Utils::Icon FIND_PRESERVE_CASE;
CORE_EXPORT extern const Utils::Icon MODE_HOME_CLASSIC;
CORE_EXPORT extern const Utils::Icon MODE_HOME_FLAT;
CORE_EXPORT extern const Utils::Icon MODE_HOME_FLAT_ACTIVE;
CORE_EXPORT extern const Utils::Icon MODE_DESIGN_CLASSIC;
CORE_EXPORT extern const Utils::Icon MODE_DESIGN_FLAT;
CORE_EXPORT extern const Utils::Icon MODE_DESIGN_FLAT_ACTIVE;

} // namespace Icons
} // namespace Core
