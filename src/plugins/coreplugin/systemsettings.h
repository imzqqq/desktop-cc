#pragma once

#include <coreplugin/dialogs/ioptionspage.h>

namespace Core {
namespace Internal {

class SystemSettings final : public IOptionsPage
{
public:
    SystemSettings();
};

} // namespace Internal
} // namespace Core
