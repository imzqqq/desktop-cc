#pragma once

namespace Utils {

namespace Constants {

const char BEAUTIFIER_SETTINGS_GROUP[] = "Beautifier";
const char BEAUTIFIER_GENERAL_GROUP[] = "General";
const char BEAUTIFIER_AUTO_FORMAT_ON_SAVE[] = "autoFormatOnSave";

} // namespace Constants
} // namespace Utils
