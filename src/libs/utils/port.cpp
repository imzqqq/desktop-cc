#include "port.h"

/*! \class Utils::Port

  \brief The Port class implements a wrapper around a 16 bit port number
  to be used in conjunction with IP addresses.
*/
